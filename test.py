import requests
from flask import  Flask, request, jsonify, g
import settings as config
from service import authorization_decorator, role_decorator
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
with app.app_context():
    print("inside app contxet")
    app.config['SQLALCHEMY_DATABASE_URI'] = config.MYSQL_DATABASE_URI
    db = SQLAlchemy(app)

# @app.before_request
# def before_request():
#     print("Before request")
#     app.config['SQLALCHEMY_DATABASE_URI'] = config.MYSQL_DATABASE_URI
#     db = SQLAlchemy(app)
#     g.db = db


# def connect_to_database():
#     db = SQLAlchemy(app)

class Book(db.Model):
    title = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)

def create_table():
    print("create table called")
    is_table_exists = False
    for table_name in db.engine.table_names():
        if table_name == 'book':
            is_table_exists = True
            break
    if not is_table_exists:
        db.create_all()


@app.route("/getbooks", methods = ['GET'])
def getBooks():
    create_table()
    books = Book.query.all()
    return "books"

@app.teardown_appcontext
def teardown_db(exception):
    # print("Inside TearDown")
    # db = getattr(g, 'db', None)
    # print("DB in teardown: ", db)
    if db is not None:
        print("Closing db", db)
        db.session.close()
        print("DB closed:", db)
if __name__ == '__main__':
    app.run(debug=True)
